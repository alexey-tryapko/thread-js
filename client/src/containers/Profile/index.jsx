import React from 'react';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import validator from 'validator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
    Grid,
    Image,
    Input,
    Button,
    Icon,
} from 'semantic-ui-react';
import { update } from './actions';

class Profile extends React.Component {
    constructor(props) {
        super(props);
        const { username, image, email, status } = props.user;
        this.state = {
            isUploading: false,
            isUsernameValid: true,
            username,
            status,
            email,
            image,
            edit: false,
        };
    }

    uploadImage = file => imageService.uploadImage(file);

    handleUploadFile = async ({ target }) => {
        this.setState({ isUploading: true });
        try {
            const { id, link } = await this.uploadImage(target.files[0]);
            this.setState({ image: { id, link }, isUploading: false });
        } catch {
            // TODO: show error
            this.setState({ isUploading: false });
        }
    }

    validateUsername = () => {
        const { username } = this.state;
        const isUsernameValid = !validator.isEmpty(username);
        this.setState({ isUsernameValid });
        return isUsernameValid;
    };

    userNameChanged = username => this.setState({ username, isUsernameValid: true });

    statusChanged = status => this.setState({ status });

    validateForm = () => [
        this.validateUsername(),
    ].every(Boolean);

    handleRemoveImage = () => this.setState({ image: null });

    handleClickSave = async () => {
        const { isUploading, username, image, status } = this.state;
        const valid = this.validateForm();
        if (!valid || isUploading) {
            return;
        }
        const imageId = image ? image.id : null;
        this.props.update({ ...this.props.user, username, imageId, image, status });
        this.setState({ edit: false });
    };

    render() {
        const { edit, username, image, status, email, isUploading, isUsernameValid } = this.state;
        return (
            <Grid container textAlign="center" style={{ paddingTop: 30 }}>
                <Grid.Column>
                    <Image centered src={getUserImgLink(image)} size="medium" circular />
                    {
                        edit && (
                            <div>
                                {

                                    image && (
                                        <Button
                                            color="red"
                                            as="label"
                                            onClick={() => this.handleRemoveImage()}
                                        >
                                            Remove image
                                        </Button>
                                    )
                                }
                                <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                                    <Icon name="image" />
                                    Attach image
                                    <input name="image" type="file" onChange={this.handleUploadFile} hidden />
                                </Button>
                            </div>
                        )
                    }
                    <br />
                    <Input
                        icon="user"
                        iconPosition="left"
                        placeholder="Username"
                        type="text"
                        disabled={!edit}
                        value={username}
                        error={!isUsernameValid}
                        onChange={ev => this.userNameChanged(ev.target.value)}
                        onBlur={this.validateUsername}
                    />
                    <br />
                    <br />
                    {
                        (status || edit) && (
                            <Input
                                placeholder="Status"
                                type="text"
                                disabled={!edit}
                                value={status}
                                onChange={ev => this.statusChanged(ev.target.value)}
                            />
                        )
                    }
                    <br />
                    <br />
                    <Input
                        icon="at"
                        iconPosition="left"
                        placeholder="Email"
                        type="email"
                        disabled
                        value={email}
                    />
                    <br />
                    <br />
                    {
                        edit ? (
                            <div>
                                <Button
                                    color="red"
                                    as="label"
                                    onClick={() => this.setState({ edit: false })}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    color="green"
                                    as="label"
                                    onClick={() => this.handleClickSave()}
                                >
                                    Save
                                </Button>
                            </div>
                        ) : (
                            <Button
                                color="blue"
                                as="label"
                                onClick={() => this.setState({ edit: true })}
                            >
                                Edit
                            </Button>
                        )
                    }

                </Grid.Column>
            </Grid>
        );
    }
}

Profile.propTypes = {
    user: PropTypes.objectOf(PropTypes.any),
    update: PropTypes.func.isRequired,
};

Profile.defaultProps = {
    user: {}
};

const mapStateToProps = rootState => ({
    user: rootState.profile.user
});

const actions = {
    update
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
