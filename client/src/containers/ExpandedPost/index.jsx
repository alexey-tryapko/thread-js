import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import {
    reactPost,
    toggleExpandedPost,
    addComment,
    toggleEditedPost,
    deletePost,
    toggleEditedComment,
    deleteComment,
    reactComment,
    togglePostReactionsList,
    toggleCommentReactionsList,
} from 'src/containers/Thread/actions';

class ExpandedPost extends React.Component {
    state = {
        open: true
    };

    closeModal = () => {
        this.props.toggleExpandedPost();
    }

    render() {
        const { post, sharePost, ...props } = this.props;
        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>
                            <Post
                                post={post}
                                reactPost={props.reactPost}
                                toggleExpandedPost={props.toggleExpandedPost}
                                sharePost={sharePost}
                                isCreator={post.userId === props.userId}
                                toggleEditedPost={props.toggleEditedPost}
                                deletePost={props.deletePost}
                                togglePostReactionsList={props.togglePostReactionsList}
                            />
                            <CommentUI.Group style={{ maxWidth: '100%' }}>
                                <Header as="h3" dividing>
                                    Comments
                                </Header>
                                {post.comments && post.comments
                                    .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                                    .map(comment => (
                                        <Comment
                                            key={comment.id}
                                            comment={comment}
                                            isCreator={comment.userId === props.userId}
                                            toggleEditedComment={props.toggleEditedComment}
                                            deleteComment={props.deleteComment}
                                            reactComment={props.reactComment}
                                            toggleCommentReactionsList={
                                                props.toggleCommentReactionsList
                                            }
                                        />
                                    ))
                                }
                                <AddComment postId={post.id} addComment={props.addComment} />
                            </CommentUI.Group>
                        </Modal.Content>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

ExpandedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    reactPost: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    toggleEditedPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    userId: PropTypes.string.isRequired,
    toggleEditedComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    reactComment: PropTypes.func.isRequired,
    togglePostReactionsList: PropTypes.func.isRequired,
    toggleCommentReactionsList: PropTypes.func.isRequired,
};

const mapStateToProps = rootState => ({
    post: rootState.posts.expandedPost,
    userId: rootState.profile.user.id,
});
const actions = {
    reactPost,
    toggleExpandedPost,
    addComment,
    toggleEditedPost,
    deletePost,
    toggleEditedComment,
    deleteComment,
    reactComment,
    togglePostReactionsList,
    toggleCommentReactionsList,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExpandedPost);
