import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditPost from 'src/components/EditPost';
import EditComment from 'src/components/EditComment';
import ReactedUsersList from 'src/components/ReactedUsersList';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
    loadPosts,
    loadMorePosts,
    reactPost,
    toggleExpandedPost,
    addPost,
    editPost,
    toggleEditedPost,
    deletePost,
    toggleEditedComment,
    editComment,
    togglePostReactionsList,
} from './actions';

import styles from './styles.module.scss';

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showOwnPosts: false,
            showAlienPosts: false,
            showLikedPosts: false,
        };
        this.postsFilter = {
            userId: undefined,
            alienPosts: false,
            likedPosts: false,
            from: 0,
            count: 10
        };
    }


    toggleAlienPosts = () => {
        const { showAlienPosts } = this.state;
        this.setState({
            showAlienPosts: !showAlienPosts,
            showOwnPosts: false,
            showLikedPosts: false,
        });
        Object.assign(this.postsFilter, {
            userId: !showAlienPosts ? this.props.userId : undefined,
            alienPosts: !showAlienPosts,
            likedPosts: false,
            from: 0
        });
        this.tooglePosts();
    };

    toggleLikedPosts = () => {
        const { showLikedPosts } = this.state;
        this.setState({
            showAlienPosts: false,
            showOwnPosts: false,
            showLikedPosts: !showLikedPosts,
        });
        Object.assign(this.postsFilter, {
            userId: !showLikedPosts ? this.props.userId : undefined,
            alienPosts: false,
            likedPosts: !showLikedPosts,
            from: 0
        });
        this.tooglePosts();
    }

    toggleOwnPosts = () => {
        const { showOwnPosts } = this.state;
        this.setState({
            showOwnPosts: !showOwnPosts,
            showAlienPosts: false,
            showLikedPosts: false,
        });
        Object.assign(this.postsFilter, {
            userId: !showOwnPosts ? this.props.userId : undefined,
            alienPosts: false,
            likedPosts: false,
            from: 0
        });
        this.tooglePosts();
    };

    tooglePosts = () => {
        this.props.loadPosts(this.postsFilter);
        this.postsFilter.from = this.postsFilter.count; // for next scroll
    };

    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    }

    sharePost = (sharedPostId) => {
        this.setState({ sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    }

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const {
            posts = [],
            expandedPost,
            hasMorePosts,
            editedPost,
            editedComment,
            reactedUsersList,
            ...props
        } = this.props;
        const { showOwnPosts, sharedPostId, showAlienPosts, showLikedPosts } = this.state;
        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost addPost={props.addPost} uploadImage={this.uploadImage} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show only my posts" checked={showOwnPosts} onChange={this.toggleOwnPosts} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show alien posts" checked={showAlienPosts} onChange={this.toggleAlienPosts} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show liked posts" checked={showLikedPosts} onChange={this.toggleLikedPosts} />
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            post={post}
                            reactPost={props.reactPost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            sharePost={this.sharePost}
                            key={post.id}
                            isCreator={post.userId === props.userId}
                            toggleEditedPost={props.toggleEditedPost}
                            deletePost={props.deletePost}
                            togglePostReactionsList={props.togglePostReactionsList}
                        />
                    ))}
                </InfiniteScroll>
                {
                    expandedPost
                    && <ExpandedPost sharePost={this.sharePost} />
                }
                {
                    sharedPostId
                    && <SharedPostLink postId={sharedPostId} close={this.closeSharePost} />
                }
                {
                    editedPost
                    && (
                        <EditPost
                            post={editedPost}
                            editPost={props.editPost}
                            toggleEditedPost={props.toggleEditedPost}
                            uploadImage={this.uploadImage}
                        />
                    )
                }
                {
                    editedComment
                    && (
                        <EditComment
                            comment={editedComment}
                            toggleEditedComment={props.toggleEditedComment}
                            editComment={props.editComment}
                        />
                    )
                }
                {
                    reactedUsersList
                    && (
                        <ReactedUsersList
                            users={reactedUsersList}
                            togglePostReactionsList={props.togglePostReactionsList}
                        />
                    )
                }
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    reactPost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    editPost: PropTypes.func.isRequired,
    editedPost: PropTypes.objectOf(PropTypes.any),
    toggleEditedPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    toggleEditedComment: PropTypes.func.isRequired,
    editedComment: PropTypes.objectOf(PropTypes.any),
    editComment: PropTypes.func.isRequired,
    togglePostReactionsList: PropTypes.func.isRequired,
    reactedUsersList: PropTypes.arrayOf(PropTypes.object),
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined,
    editedPost: undefined,
    editedComment: undefined,
    reactedUsersList: undefined,
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user.id,
    editedPost: rootState.posts.editedPost,
    editedComment: rootState.posts.editedComment,
    reactedUsersList: rootState.posts.reactedUsersList,
});

const actions = {
    loadPosts,
    loadMorePosts,
    reactPost,
    toggleExpandedPost,
    addPost,
    editPost,
    toggleEditedPost,
    deletePost,
    toggleEditedComment,
    editComment,
    togglePostReactionsList,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
