import {
    SET_ALL_POSTS,
    LOAD_MORE_POSTS,
    ADD_POST,
    SET_EXPANDED_POST,
    EDIT_POST,
    SET_EDITED_POST,
    DELETE_POST,
    SET_EDITED_COMMENT,
    SET_REACTIONS_LIST,
} from './actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_POSTS:
            return {
                ...state,
                posts: action.posts,
                hasMorePosts: Boolean(action.posts.length)
            };
        case LOAD_MORE_POSTS:
            return {
                ...state,
                posts: [...(state.posts || []), ...action.posts],
                hasMorePosts: Boolean(action.posts.length)
            };
        case ADD_POST:
            return {
                ...state,
                posts: [action.post, ...state.posts]
            };
        case EDIT_POST:
            return {
                ...state,
                posts: state.posts.map(post => (post.id === action.post.id ? action.post : post))
            };
        case SET_EXPANDED_POST:
            return {
                ...state,
                expandedPost: action.post
            };
        case SET_EDITED_POST:
            return {
                ...state,
                editedPost: action.post
            };
        case SET_EDITED_COMMENT:
            return {
                ...state,
                editedComment: action.comment
            };
        case SET_REACTIONS_LIST:
            return {
                ...state,
                reactedUsersList: action.reactions,
            };
        case DELETE_POST:
            return {
                ...state,
                posts: state.posts.filter(post => post.id !== action.postId)
            };
        default:
            return state;
    }
};
