import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    EDIT_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST,
    SET_EDITED_POST,
    DELETE_POST,
    SET_EDITED_COMMENT,
    SET_REACTIONS_LIST,
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const editPostAction = post => ({
    type: EDIT_POST,
    post
});

const setEditedPostAction = post => ({
    type: SET_EDITED_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

const setEditedCommentAction = comment => ({
    type: SET_EDITED_COMMENT,
    comment
});

const deletePostAction = postId => ({
    type: DELETE_POST,
    postId
});

const setReactionsListAction = reactions => ({
    type: SET_REACTIONS_LIST,
    reactions
});

export const loadPosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts
        .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const toggleEditedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setEditedPostAction(post));
};

export const toggleEditedComment = commentId => async (dispatch) => {
    const comment = commentId ? await commentService.getComment(commentId) : undefined;
    dispatch(setEditedCommentAction(comment));
};

export const togglePostReactionsList = (postId, type) => async (dispatch) => {
    const filter = { postId, type };
    let list;
    if (postId) {
        const res = await postService.getReactions(filter);
        list = res.map(({ user }) => user);
    } else {
        list = undefined;
    }
    dispatch(setReactionsListAction(list));
};

export const toggleCommentReactionsList = (commentId, type) => async (dispatch) => {
    const filter = { commentId, type };
    let list;
    if (commentId) {
        const res = await commentService.getReactions(filter);
        list = res.map(({ user }) => user);
    } else {
        list = undefined;
    }
    dispatch(setReactionsListAction(list));
};

export const reactPost = (postId, type) => async (dispatch, getRootState) => {
    const diff = await postService.reactPost(postId, type);
    const { isLike: likeDiff, isDislike: dislikeDiff } = diff;

    const mapReaction = post => ({
        ...post,
        likeCount: Number(post.likeCount) + likeDiff, // diff is taken from the current closure
        dislikeCount: Number(post.dislikeCount) + dislikeDiff,
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapReaction(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapReaction(expandedPost)));
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const editPost = request => async (dispatch, getRootState) => {
    const { id } = await postService.updatePost(request.id, request);
    const post = await postService.getPost(id);
    dispatch(editPostAction(post));

    const { posts: { expandedPost } } = getRootState();

    if (expandedPost && expandedPost.id === id) {
        dispatch(setExpandedPostAction(post));
    }
};

export const deletePost = postId => async (dispatch, getRootState) => {
    const res = postService.deletePost(postId);
    if (!res) return;
    dispatch(deletePostAction(postId));
    const { posts: { expandedPost } } = getRootState();

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(undefined));
    }
};

export const editComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.updateComment(request.id, request);
    const updatedComment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        comments: [...(post.comments || [])].map(comment => (
            comment.id === id ? updatedComment : comment
        ))
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== updatedComment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === updatedComment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
    const removedComment = await commentService.getComment(commentId);
    const res = commentService.deleteComment(commentId);
    if (!res) return;
    const mapComments = post => ({
        ...post,
        comments: [...(post.comments || [])].filter(comment => comment.id !== commentId)
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== removedComment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === removedComment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const reactComment = (commentId, type) => async (dispatch, getRootState) => {
    const reactedComment = await commentService.getComment(commentId);
    const diff = await commentService.reactComment(commentId, type);
    const { isLike: likeDiff, isDislike: dislikeDiff } = diff;

    const mapReaction = comment => ({
        ...comment,
        likeCount: Number(comment.likeCount) + likeDiff, // diff is taken from the current closure
        dislikeCount: Number(comment.dislikeCount) + dislikeDiff,
    });

    const mapComments = post => ({
        ...post,
        comments: [...(post.comments || [])].map(comment => (
            comment.id === commentId ? mapReaction(comment) : comment
        ))
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== reactedComment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === reactedComment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};
