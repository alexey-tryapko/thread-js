import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/comments',
        type: 'POST',
        request
    });
    return response.json();
};

export const getComment = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'GET'
    });
    return response.json();
};

export const updateComment = async (id, request) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'PUT',
        request
    });
    return response.json();
};

export const deleteComment = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'DELETE',
    });
    return response.ok;
};

export const reactComment = async (commentId, type) => {
    const response = await callWebApi({
        endpoint: '/api/comments/react',
        type: 'PUT',
        request: {
            commentId,
            type
        }
    });
    return response.json();
};

export const getReactions = async (filter) => {
    const response = await callWebApi({
        endpoint: '/api/comments/react',
        type: 'GET',
        query: filter
    });
    return response.json();
};
