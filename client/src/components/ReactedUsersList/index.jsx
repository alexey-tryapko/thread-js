import React from 'react';
import PropTypes from 'prop-types';
import { Modal, List, Image } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const ReactedUsersList = (props) => {
    const { users, togglePostReactionsList } = props;
    const closeModal = () => togglePostReactionsList();

    return (
        <Modal dimmer="blurring" open onClose={closeModal} className={styles.usersModal}>
            <Modal.Header className={styles.header}>
                <span>Users</span>
            </Modal.Header>
            <List animated verticalAlign="middle">
                {
                    users.map(({ id, username, image }) => (
                        <List.Item key={id} style={{ marginBottom: '1rem' }}>
                            <Image avatar src={getUserImgLink(image)} />
                            <List.Content>
                                <List.Header>{username}</List.Header>
                            </List.Content>
                        </List.Item>
                    ))
                }
            </List>
        </Modal>
    );
};
ReactedUsersList.propTypes = {
    users: PropTypes.arrayOf(PropTypes.object).isRequired,
    togglePostReactionsList: PropTypes.func.isRequired,
};

export default ReactedUsersList;
