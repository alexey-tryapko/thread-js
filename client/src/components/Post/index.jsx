import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({
    post,
    reactPost,
    toggleExpandedPost,
    sharePost,
    isCreator,
    toggleEditedPost,
    deletePost,
    togglePostReactionsList,
}) => {
    const {
        id,
        image,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt
    } = post;
    const date = moment(createdAt).fromNow();

    const creatorActions = () => (
        <div className={styles.creatorActionWrapper}>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleEditedPost(id)}>
                <Icon name="edit" />
            </Label>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
                <Icon name="trash alternate" />
            </Label>
        </div>
    );
    return (
        <Card style={{ width: '100%' }}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by
                        {' '}
                        {user.username}
                        {' - '}
                        {date}
                    </span>
                </Card.Meta>
                <Card.Description>
                    {body}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => reactPost(id, 'like')}>
                    <Icon name="thumbs up" />
                    {likeCount}
                </Label>
                {
                    +likeCount !== 0
                    && (
                        <Label basic size="small" as="a" className={styles.toolbarBtnReaction} onClick={() => togglePostReactionsList(id, 'isLike')}>
                            <Icon name="users" />
                        </Label>
                    )
                }
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => reactPost(id, 'dislike')}>
                    <Icon name="thumbs down" />
                    {dislikeCount}
                </Label>
                {
                    +dislikeCount !== 0
                    && (
                        <Label basic size="small" as="a" className={styles.toolbarBtnReaction} onClick={() => togglePostReactionsList(id, 'isDislike')}>
                            <Icon name="users" />
                        </Label>
                    )
                }
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                    <Icon name="share alternate" />
                </Label>
                {
                    isCreator
                    && creatorActions()
                }
            </Card.Content>
        </Card>
    );
};


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    reactPost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    isCreator: PropTypes.bool.isRequired,
    toggleEditedPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    togglePostReactionsList: PropTypes.func.isRequired,
};

export default Post;
