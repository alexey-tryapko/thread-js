import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = (props) => {
    const {
        comment: { id, body, createdAt, user, likeCount, dislikeCount },
        isCreator,
        toggleEditedComment,
        deleteComment,
        reactComment,
        toggleCommentReactionsList,
    } = props;
    const date = moment(createdAt).fromNow();

    const creatorActions = () => (
        <div className={styles.creatorActionWrapper}>
            <CommentUI.Action onClick={() => toggleEditedComment(id)}>
                <Icon name="edit" />
            </CommentUI.Action>
            <CommentUI.Action onClick={() => deleteComment(id)}>
                <Icon name="trash alternate" />
            </CommentUI.Action>
        </div>
    );
    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                </CommentUI.Metadata>
                <CommentUI.Text>
                    {body}
                </CommentUI.Text>
                <CommentUI.Actions>
                    <CommentUI.Action onClick={() => reactComment(id, 'like')}>
                        <Icon name="thumbs up" />
                        {likeCount}
                    </CommentUI.Action>
                    {
                        +likeCount !== 0
                        && (
                            <CommentUI.Action onClick={() => toggleCommentReactionsList(id, 'isLike')}>
                                <Icon name="users" />
                            </CommentUI.Action>
                        )
                    }
                    <CommentUI.Action onClick={() => reactComment(id, 'dislike')}>
                        <Icon name="thumbs down" />
                        {dislikeCount}
                    </CommentUI.Action>
                    {
                        +dislikeCount !== 0
                        && (
                            <CommentUI.Action onClick={() => toggleCommentReactionsList(id, 'isDislike')}>
                                <Icon name="users" />
                            </CommentUI.Action>
                        )
                    }
                    {
                        isCreator
                        && creatorActions()
                    }
                </CommentUI.Actions>
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    isCreator: PropTypes.bool.isRequired,
    toggleEditedComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    reactComment: PropTypes.func.isRequired,
    toggleCommentReactionsList: PropTypes.func.isRequired,
};

export default Comment;
