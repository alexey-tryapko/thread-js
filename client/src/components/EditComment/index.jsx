import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

class EditComment extends React.Component {
    constructor(props) {
        super(props);
        const { body } = props.comment;
        this.state = { body };
    }

    editCommentHandle = async () => {
        const { body } = this.state;
        if (!body) {
            return;
        }
        this.props.editComment({ ...this.props.comment, body });
        this.props.toggleEditedComment();
    }

    closeModal = () => this.props.toggleEditedComment();

    render() {
        const { body } = this.state;
        return (
            <Modal dimmer="blurring" open onClose={this.closeModal}>
                <Modal.Header className={styles.header}>
                    <span>Edit Comment</span>
                </Modal.Header>
                <Segment>
                    <Form onSubmit={this.editCommentHandle}>
                        <Form.TextArea
                            name="body"
                            value={body}
                            placeholder="Type a comment..."
                            onChange={ev => this.setState({ body: ev.target.value })}
                        />
                        <Button floated="right" color="blue" type="submit" className={styles.btnSave}>Save</Button>
                    </Form>
                </Segment>
            </Modal>
        );
    }
}

EditComment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleEditedComment: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
};

export default EditComment;
