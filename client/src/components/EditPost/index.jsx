import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button, Segment, Image, Icon } from 'semantic-ui-react';

import styles from './styles.module.scss';

class EditPost extends React.Component {
    constructor(props) {
        super(props);
        const { body, image } = props.post;
        this.state = {
            isUploading: false,
            body,
            image
        };
    }

    handleUploadFile = async ({ target }) => {
        this.setState({ isUploading: true });
        try {
            const { id, link } = await this.props.uploadImage(target.files[0]);
            this.setState({ image: { id, link }, isUploading: false });
        } catch {
            // TODO: show error
            this.setState({ isUploading: false });
        }
    }

    editPostHandle = async () => {
        const { body, image } = this.state;
        if (!body) {
            return;
        }
        const imageId = image ? image.id : null;
        this.props.editPost({ ...this.props.post, body, imageId, image });
        this.props.toggleEditedPost();
    }

    closeModal = () => this.props.toggleEditedPost();

    handleRemoveImage = () => this.setState({ image: null });

    render() {
        const { body, image, isUploading } = this.state;
        return (
            <Modal dimmer="blurring" open onClose={this.closeModal}>
                <Modal.Header className={styles.header}>
                    <span>Edit Post</span>
                </Modal.Header>
                <Segment>
                    <Form onSubmit={this.editPostHandle}>
                        <Form.TextArea
                            name="body"
                            value={body}
                            placeholder="What is the news?"
                            onChange={ev => this.setState({ body: ev.target.value })}
                        />
                        {image && (
                            <div className={styles.imageWrapper}>
                                <Image className={styles.image} src={image.link} alt="post" />
                            </div>
                        )}
                        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                            <Icon name="image" />
                            Attach image
                            <input name="image" type="file" onChange={this.handleUploadFile} hidden />
                        </Button>
                        {
                            image && (
                                <Button color="red" as="label" onClick={() => this.handleRemoveImage()}>Remove image</Button>
                            )
                        }
                        <Button floated="right" color="blue" type="submit">Save</Button>
                    </Form>
                </Segment>
            </Modal>
        );
    }
}

EditPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleEditedPost: PropTypes.func.isRequired,
    editPost: PropTypes.func.isRequired,
    uploadImage: PropTypes.func.isRequired,
};

export default EditPost;
