import commentRepository from '../../data/repositories/comment.repository';
import commentReactionRepository from '../../data/repositories/comment-reaction.repository';

export const create = (userId, comment) => commentRepository.create({
    ...comment,
    userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const update = async (commentId, userId, comment) => (
    (userId === comment.userId && commentId === comment.id)
        && commentRepository.updateById(commentId, comment)
);

export const deleteComment = async (commentId, userId) => {
    const comment = await commentRepository.getCommentById(commentId);
    return (comment && userId === comment.user.id)
        ? commentRepository.deleteById(commentId)
        : false;
};

export const setReaction = async (userId, { commentId, type }) => {
    // define the callback for future use as a promise
    const types = ['isLike', 'isDislike'];
    const [reactType, anotherReact] = type === 'like' ? types : types.reverse();

    const updateOrDelete = react => (react[reactType] === true
        ? commentReactionRepository.deleteById(react.id)
        : commentReactionRepository.updateById(
            react.id,
            { [reactType]: true, [anotherReact]: false }
        )
    );

    const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

    const result = reaction
        ? await updateOrDelete(reaction)
        : await commentReactionRepository.create({
            userId,
            commentId,
            [reactType]: true,
            [anotherReact]: false
        });

    let diff;
    if (!reaction) {
        diff = { [reactType]: 1, [anotherReact]: 0 };
    } else {
        diff = Number.isInteger(result)
            ? { [reactType]: -1, [anotherReact]: 0 }
            : { [reactType]: 1, [anotherReact]: -1 };
    }
    return diff;
};

export const getCommentReactions = filter => (
    commentReactionRepository.getAllCommentReactions(filter)
);
