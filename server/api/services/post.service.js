import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const setReaction = async (userId, { postId, type }) => {
    // define the callback for future use as a promise
    const types = ['isLike', 'isDislike'];
    const [reactType, anotherReact] = type === 'like' ? types : types.reverse();

    const updateOrDelete = react => (react[reactType] === true
        ? postReactionRepository.deleteById(react.id)
        : postReactionRepository.updateById(
            react.id,
            { [reactType]: true, [anotherReact]: false }
        )
    );

    const reaction = await postReactionRepository.getPostReaction(userId, postId);

    const result = reaction
        ? await updateOrDelete(reaction)
        : await postReactionRepository.create({
            userId,
            postId,
            [reactType]: true,
            [anotherReact]: false
        });

    let diff;
    if (!reaction) {
        diff = { [reactType]: 1, [anotherReact]: 0 };
    } else {
        diff = Number.isInteger(result)
            ? { [reactType]: -1, [anotherReact]: 0 }
            : { [reactType]: 1, [anotherReact]: -1 };
    }
    return diff;
};

export const update = async (postId, userId, post) => (
    (userId === post.userId && postId === post.id)
        && postRepository.updateById(postId, post)
);

export const deletePost = async (postId, userId) => {
    const post = await postRepository.getPostById(postId);
    return (post && userId === post.user.id) ? postRepository.deleteById(postId) : false;
};

export const getPostReactions = filter => (
    postReactionRepository.getAllPostReactions(filter)
);
