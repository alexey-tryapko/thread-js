import { Router } from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(diff => res.send(diff))
        .catch(next))
    .get('/react/', (req, res, next) => commentService.getCommentReactions(req.query)
        .then(list => res.send(list))
        .catch(next))
    .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
        .then(comment => res.send(comment))
        .catch(next))
    .put('/:id', (req, res, next) => commentService.update(req.params.id, req.user.id, req.body)
        .then((comment) => {
            req.io.emit('comment_updated', comment);
            return res.send(comment);
        })
        .catch(next))
    .delete('/:id', (req, res, next) => commentService.deleteComment(req.params.id, req.user.id)
        .then((result) => {
            if (result) {
                req.io.emit('comment_deleted', result);
                return res.sendStatus(200);
            }
            return res.sendStatus(400);
        })
        .catch(next))
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(comment => res.send(comment))
        .catch(next));

export default router;
