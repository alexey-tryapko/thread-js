import { Router } from 'express';
import * as postService from '../services/post.service';

const router = Router();

router
    .get('/', (req, res, next) => postService.getPosts(req.query)
        .then(posts => res.send(posts))
        .catch(next))
    .get('/react/', (req, res, next) => postService.getPostReactions(req.query)
        .then(list => res.send(list))
        .catch(next))
    .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
        .then(post => res.send(post))
        .catch(next))
    .post('/', (req, res, next) => postService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then((post) => {
            req.io.emit('new_post', post); // notify all users that a new post was created
            return res.send(post);
        })
        .catch(next))
    .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(diff => res.send(diff))
        .catch(next))
    .delete('/:id', (req, res, next) => postService.deletePost(req.params.id, req.user.id)
        .then((result) => {
            if (result) {
                req.io.emit('post_deleted', result);
                return res.sendStatus(200);
            }
            return res.sendStatus(400);
        })
        .catch(next))
    .put('/:id', (req, res, next) => postService.update(req.params.id, req.user.id, req.body)
        .then((post) => {
            req.io.emit('post_updated', post);
            return res.send(post);
        })
        .catch(next));

export default router;
