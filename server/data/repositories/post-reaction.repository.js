import { PostReactionModel, PostModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './base.repository';

class PostReactionRepository extends BaseRepository {
    getPostReaction(userId, postId) {
        return this.model.findOne({
            group: [
                'postReaction.id',
                'post.id'
            ],
            where: { userId, postId },
            include: [{
                model: PostModel,
                attributes: ['id', 'userId']
            }]
        });
    }

    getAllPostReactions(filter) {
        const {
            postId,
            type,
        } = filter;
        return this.model.findAll({
            group: [
                'postReaction.id',
                'user.id',
                'user->image.id'
            ],
            where: {
                postId,
                [type]: true,
            },
            include: [{
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }],
        });
    }
}

export default new PostReactionRepository(PostReactionModel);
