import { Op } from 'sequelize';
import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel, CommentReactionModel } from '../models/index';
import BaseRepository from './base.repository';

const postLikeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
const commentLikeCase = bool => `(SELECT COUNT(*) FROM "commentReactions"
                                WHERE "commentId" = "comments"."id" AND "isLike" = ${bool})`;

class PostRepository extends BaseRepository {
    async getPosts(filter) {
        const {
            from: offset,
            count: limit,
            userId,
            alienPosts,
            likedPosts,
        } = filter;

        const where = {};
        const filterByLikedPosts = {
            '$postReactions.userId$': `${userId}`,
            '$postReactions.isLike$': true,
        };

        const filterByAuthor = alienPosts === 'true' ? { userId: { [Op.ne]: userId } } : { userId };
        if (userId && likedPosts === 'false') {
            Object.assign(where, filterByAuthor);
        }
        if (userId && likedPosts === 'true') {
            Object.assign(where, filterByLikedPosts);
        }

        return this.model.findAll({
            where,
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(postLikeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(postLikeCase(false))), 'dislikeCount']
                ]
            },
            include: [{
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: PostReactionModel,
                attributes: [],
                duplicating: false
            }],
            group: [
                'post.id',
                'image.id',
                'user.id',
                'user->image.id',
            ],
            order: [['createdAt', 'DESC']],
            offset,
            limit
        });
    }

    getPostById(id) {
        return this.model.findOne({
            group: [
                'post.id',
                'comments.id',
                'comments->user.id',
                'comments->user->image.id',
                'comments->commentReactions.id',
                'user.id',
                'user->image.id',
                'image.id',
            ],
            where: { id },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(postLikeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(postLikeCase(false))), 'dislikeCount']
                ]
            },
            include: [{
                model: CommentModel,
                include: [{
                    model: UserModel,
                    attributes: ['id', 'username'],
                    include: {
                        model: ImageModel,
                        attributes: ['id', 'link']
                    }
                }, {
                    model: CommentReactionModel,
                    attributes: []
                }],
                attributes: {
                    include: [
                        [sequelize.literal(commentLikeCase(true)), 'likeCount'],
                        [sequelize.literal(commentLikeCase(false)), 'dislikeCount'],
                    ]
                }
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: PostReactionModel,
                attributes: [],
            }]
        });
    }
}

export default new PostRepository(PostModel);
