export default {
    up: (queryInterface, Sequelize) => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.addColumn(
                'comments',
                'deletedAt',
                {
                    type: Sequelize.DATE,
                    allowNull: true
                },
                { transaction }
            )
        ])),

    down: queryInterface => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.removeColumn('comments', 'deletedAt', { transaction }),
        ]))
};
